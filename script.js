"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/";
var gOrderRequest = {
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    loaiPizza: "",
    idVourcher: "",
    idLoaiNuocUong: "",
    soLuongNuoc: "",
    hoTen: "",
    thanhTien: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: ""
};
var gDiscountPercent = 0;
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    //Gắn sự kiện cho các nút chọn combo
    $("#btn-small").click(function () {
        onBtnSmallClick();
    });
    $("#btn-medium").click(function () {
        onBtnMediumClick();
    });
    $("#btn-large").click(function () {
        onBtnLargeClick();
    });
    //Gán sự kiện cho các nút chọn loại pizza:
    $("#btn-hawaii").click(function () {
        onBtnHawaiiClick();
    });
    $("#btn-seafood").click(function () {
        onBtnSeafoodClick();
    });
    $("#btn-bacon").click(function () {
        onBtnBaconClick();
    });
    //Gán sự kiện nút Gửi:
    $("#btn-send").click(function () {
        onBtnSendClick()
    });
    //Không load lại trang khi submit form:
    $('form').submit(function (event) {
        event.preventDefault();
    });
    //gán sự kiện nút tạo đơn:
    $("#btn-create-order").click(function(){
        onBtnCreateOrderClick();
    })
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//Hàm xử lý khi load trang:
function onPageLoading() {
    loadSelectDrinks();
   
}
//hàm xử lý sự kiện nút combo small được chọn:
function onBtnSmallClick() {
    gOrderRequest.kichCo = "S";
    gOrderRequest.duongKinh = "20";
    gOrderRequest.suon = "2";
    gOrderRequest.salad = "200";
    gOrderRequest.soLuongNuoc = "2";
    gOrderRequest.thanhTien = "150000";
    console.log("Combo selected: " + gOrderRequest.kichCo);
    $("#btn-small")
        .removeClass("btn-warning")
        .addClass("btn-success")
        .attr("data-is-selected-menu", "Y");
    $("#btn-medium")
        .removeClass("btn-success")
        .addClass("btn-warning")
        .attr("data-is-selected-menu", "N");
    $("#btn-large")
        .removeClass("btn-success")
        .addClass("btn-warning")
        .attr("data-is-selected-menu", "N");
}
//hàm xử lý sự kiện nút combo medium được chọn:
function onBtnMediumClick() {
    gOrderRequest.kichCo = "M";
    gOrderRequest.duongKinh = "25";
    gOrderRequest.suon = "4";
    gOrderRequest.salad = "300";
    gOrderRequest.soLuongNuoc = "3";
    gOrderRequest.thanhTien = "200000";
    console.log("Combo selected: " + gOrderRequest.kichCo);
    $("#btn-small")
        .removeClass("btn-success")
        .addClass("btn-warning")
        .attr("data-is-selected-menu", "N");
    $("#btn-medium")
        .removeClass("btn-warning")
        .addClass("btn-success")
        .attr("data-is-selected-menu", "Y");
    $("#btn-large")
        .removeClass("btn-success")
        .addClass("btn-warning")
        .attr("data-is-selected-menu", "N");
}
//hàm xử lý sự kiện nút combo large được chọn:
function onBtnLargeClick() {
    gOrderRequest.kichCo = "L";
    gOrderRequest.duongKinh = "30";
    gOrderRequest.suon = "8";
    gOrderRequest.salad = "500";
    gOrderRequest.soLuongNuoc = "4";
    gOrderRequest.thanhTien = "250000";
    console.log("Combo selected: " + gOrderRequest.kichCo);
    $("#btn-small")
        .removeClass("btn-success")
        .addClass("btn-warning")
        .attr("data-is-selected-menu", "N");
    $("#btn-medium")
        .removeClass("btn-success")
        .addClass("btn-warning")
        .attr("data-is-selected-menu", "N");
    $("#btn-large")
        .removeClass("btn-warning")
        .addClass("btn-success")
        .attr("data-is-selected-menu", "Y");
}
//hàm xử lý sự kiện loại nút seafood được chọn:
function onBtnSeafoodClick() {
    gOrderRequest.loaiPizza = "SEAFOOD";
    console.log("Loại pizza được chọn: " + gOrderRequest.loaiPizza);
    $("#btn-seafood")
        .removeClass("btn-warning")
        .addClass("btn-success")
        .attr("data-is-selected-menu", "Y");
    $("#btn-hawaii")
        .removeClass("btn-success")
        .addClass("btn-warning")
        .attr("data-is-selected-menu", "N");
    $("#btn-bacon")
        .removeClass("btn-success")
        .addClass("btn-warning")
        .attr("data-is-selected-menu", "N");
}
//hàm xử lý sự kiện loại nút seafood được chọn:
function onBtnHawaiiClick() {
    gOrderRequest.loaiPizza = "HAWAII";
    console.log("Loại pizza được chọn: " + gOrderRequest.loaiPizza);
    $("#btn-seafood")
        .removeClass("btn-success")
        .addClass("btn-warning")
        .attr("data-is-selected-menu", "N");
    $("#btn-hawaii")
        .removeClass("btn-warning")
        .addClass("btn-success")
        .attr("data-is-selected-menu", "Y");
    $("#btn-bacon")
        .removeClass("btn-success")
        .addClass("btn-warning")
        .attr("data-is-selected-menu", "N");
}
//hàm xử lý sự kiện loại nút seafood được chọn:
function onBtnBaconClick() {
    gOrderRequest.loaiPizza = "BACON";
    console.log("Loại pizza được chọn: " + gOrderRequest.loaiPizza);
    $("#btn-seafood")
        .removeClass("btn-success")
        .addClass("btn-warning")
        .attr("data-is-selected-menu", "N");
    $("#btn-hawaii")
        .removeClass("btn-success")
        .addClass("btn-warning")
        .attr("data-is-selected-menu", "N");
    $("#btn-bacon")
        .removeClass("btn-warning")
        .addClass("btn-success")
        .attr("data-is-selected-menu", "Y");
}
//Hàm xử lý sự kiện nút Gửi:
function onBtnSendClick() {
    //B1: Thu thập dữ liệu - Lưu ở biến global:
    getFullDataOrder();
    //B2: Kiểm tra dữ liệu:
    var vIsValidate = validateDataOrder();
    if (vIsValidate) {
        loadDataToModalForm();
        $("#modal-confirm-order").modal('show');
    }

   
}
//Hàm xử lý sk nút Tạo đơn được ấn:
function onBtnCreateOrderClick(){
    $("#modal-confirm-order").modal('hide');
    $.ajax({
        url: gBASE_URL + "/orders",
        type: "POST",
        dataType: "json",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(gOrderRequest),
        success: function(paramResponse){
            // console.log(paramResponse);
            $("#inp-ordercode").val(paramResponse.orderCode)
            $("#modal-result").modal('show');
        },
        error: function(){
            alert("Tạo không thành công");
        }

    })
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

//Hàm load dữ liệu ra select đồ uống
function loadSelectDrinks() {
    $.ajax({
        url: gBASE_URL + "/drinks",
        type: "GET",
        dataType: "json",
        success: function (paramResponse) {
            // console.log(paramResponse);
            handleDataDrinks(paramResponse);
        },
        error: function (ajaxContext) {
            console.log(ajaxContext);
        }
    });
}
//Hàm xử lý thẻ select Drink:
function handleDataDrinks(paramDataDrink) {
    var vSelectDrink = $("#select-drink");
    for (var bI = 0; bI < paramDataDrink.length; bI++) {
        $("<option></option>", {
            value: paramDataDrink[bI].maNuocUong,
            text: paramDataDrink[bI].tenNuocUong
        }).appendTo(vSelectDrink);
    }
}
//hàm lấy phần trăm giảm giá:
function getDiscountPercent(paramVoucherId) {
    $.ajax({
        url: gBASE_URL + "voucher_detail/" + paramVoucherId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function (paramResponse) {
            // debugger;
            console.log(paramResponse);
            gDiscountPercent = paramResponse.phanTramGiamGia;
        },
        error: function () {
            console.log("Không tìm thấy voucher " + paramVoucherId);
        }
    });
}
//Hàm lấy full dữ liệu đơn hàng; 
function getFullDataOrder() {
    gOrderRequest.idLoaiNuocUong = $("#select-drink").val();
    gOrderRequest.email = $("#inp-email").val().trim();
    gOrderRequest.hoTen = $("#inp-name").val().trim();
    gOrderRequest.diaChi = $("#inp-address").val().trim();
    gOrderRequest.soDienThoai = $("#inp-phone-number").val().trim();
    gOrderRequest.loiNhan = $("#inp-message").val().trim();
    gOrderRequest.idVourcher = $("#inp-voucher-id").val().trim();
}
//hàm kiểm tra thông tin nhập vào:
function validateDataOrder() {
    if (gOrderRequest.kichCo == "") {
        alert("Vui lòng chọn combo Size !");
        return false;
    }
    if (gOrderRequest.loaiPizza == "") {
        alert("Vui lòng chọn loại Pizza!");
        return false;
    }
    if (gOrderRequest.idLoaiNuocUong == "all") {
        alert("Vui lòng chọn loại đồ uống!");
        return false;
    }
    if (gOrderRequest.hoTen == "") {
        alert("Vui lòng nhập họ và tên!");
        return false;
    }
    if (!checkEmail(gOrderRequest.email)) {
        return false;
    }
    if (gOrderRequest.soDienThoai == "") {
        alert("Vui lòng nhập số điện thoại!");
        return false;
    }
    if (isNaN(gOrderRequest.soDienThoai)) {
        alert("Số điện thoại phải là số!");
        return false;
    }
    if (gOrderRequest.diaChi == "") {
        alert("Vui lòng nhập địa chỉ !");
        return false;
    }
    return true;
}
//Hàm kiểm tra email:
function checkEmail(paramEmail) {
    var vValidRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (paramEmail == "") {
      alert("Vui lòng nhập email !");
      return false;
    }
    if (paramEmail.match(vValidRegex)) {
      return true;
    } else {
      alert("Vui lòng nhập đúng email !");
      return false;
    }
  }
  //Hàm load dữ liệu ra form modal:
  function loadDataToModalForm(){
    getDiscountPercent(gOrderRequest.idVourcher);
    console.log(gDiscountPercent)
    var vPhaiThanhToan =gOrderRequest.thanhTien - gOrderRequest.thanhTien * gDiscountPercent / 100;
    $("#inp-modal-name").val(gOrderRequest.hoTen);
    $("#inp-modal-phone-number").val(gOrderRequest.soDienThoai);
    $("#inp-modal-address").val(gOrderRequest.diaChi);
    $("#inp-modal-message").val(gOrderRequest.loiNhan);
    $("#inp-modal-voucher-id").val(gOrderRequest.idVourcher);
    $("#inp-modal-detail").val("Xác nhận: " + gOrderRequest.hoTen + ", " + gOrderRequest.soDienThoai + ", " + gOrderRequest.diaChi
        + "\n" + "Menu " + gOrderRequest.kichCo + ", sườn nướng  " + gOrderRequest.suon + ", nước " + gOrderRequest.soLuongNuoc + ",..."
        + "\n" + "Loại pizza: " + gOrderRequest.loaiPizza + ", Giá: " + gOrderRequest.thanhTien + "vnd, mã giảm giá: " + gOrderRequest.idVourcher
        + "\n" + "Phải thanh toán: " + vPhaiThanhToan + "vnd (giảm giá " + gDiscountPercent + "%)");
  }